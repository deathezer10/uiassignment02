﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharDeleterDialog : MonoBehaviour
{

    GameObject[] objToDelete;

    public void Hide()
    {
        gameObject.SetActive(false);
        objToDelete[1].transform.Find("Cross").gameObject.SetActive(false);
    }

    public void Show(GameObject[] objectsToDelete)
    {
        gameObject.SetActive(true);
        objToDelete = objectsToDelete;
    }

    // Confirm delete character
    public void OnConfirm()
    {
        foreach (GameObject go in objToDelete)
        {
            DestroyObject(go);
        }

        objToDelete = null;

        var lblCharName = GameObject.Find("lblCharName").GetComponent<Text>();

        // Delete character from playerprefs
        List<string> characters = new List<string>(PlayerPrefsX.GetStringArray("Characters"));
        characters.Remove(lblCharName.text);
        PlayerPrefsX.SetStringArray("Characters", characters.ToArray());

        // Remove name
        lblCharName.text = "";
        GameObject.Find("btnPlay").GetComponent<Image>().enabled = false;
        GameObject.Find("btnPlay").GetComponent<Button>().enabled = false;
        
        gameObject.SetActive(false);
    }

}
