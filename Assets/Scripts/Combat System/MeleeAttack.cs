﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MeleeAttack : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    public Animator PlayerModel;

    public void OnPointerDown(PointerEventData eventData)
    {
        PlayerModel.SetBool("isSwinging", true);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        PlayerModel.SetBool("isSwinging", false);
    }

    // Update is called once per frame
    void Update () {
        		
	}
}
