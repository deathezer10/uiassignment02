﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class ApplyCooldown : MonoBehaviour, IPointerClickHandler
{

    public float CooldownTime = 2;

    float CurrentCooldownTime;

    bool isOnCooldown = false;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (CurrentCooldownTime == CooldownTime)
        {
            Transform child = transform.GetChild(0);
            child.gameObject.SetActive(true);
            GetComponent<Button>().interactable = false;
            isOnCooldown = true;
        }
    }

    void Start()
    {
        CurrentCooldownTime = CooldownTime;
    }

    void Update()
    {

        if (isOnCooldown)
        {
            Transform child = transform.GetChild(0);

            CurrentCooldownTime -= Time.deltaTime;

            if (CurrentCooldownTime <= 0)
            {
                CurrentCooldownTime = CooldownTime;
                isOnCooldown = false;
                child.gameObject.SetActive(false);
                GetComponent<Button>().interactable = true;
            }

            child.GetComponent<Image>().fillAmount = CurrentCooldownTime / CooldownTime;
        }

    }
}
