﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{

    public Transform PlayerModel;
    public Camera PlayerCamera;
    public CharacterController CController;

    public RectTransform JoystickBG;
    Transform JoystickFG;
    
    Vector3 moveDirection = Vector3.zero;


    public void OnPointerDown(BaseEventData data)
    {
        PointerEventData pData = (PointerEventData)data;

        // Start with touch position
        JoystickBG.position = pData.position;
        JoystickBG.GetChild(0).position = pData.position;
    }

    public void OnPointerDrag(BaseEventData data)
    {
        PointerEventData pData = (PointerEventData)data;
        JoystickFG = JoystickBG.transform.GetChild(0);

        JoystickFG.position = pData.position;

        // Limit the distance that the joystick foreground can move
        Vector3 direction = (JoystickFG.position - JoystickBG.position);
        Vector3 clampedDirection = Vector3.ClampMagnitude(direction, JoystickBG.rect.size.x / 2); // This can be used to feed character controller on where to move

        JoystickFG.position = clampedDirection + JoystickBG.position;

        moveDirection = clampedDirection;

    }


    // Update is called once per frame
    void Update()
    {
        const float SPEED = 5;
        Animator charAnimation = CController.gameObject.GetComponent<Animator>();

        if (JoystickBG.gameObject.activeInHierarchy)
        {

            float angle = Mathf.Atan2(moveDirection.x, moveDirection.y) * Mathf.Rad2Deg;
            CController.gameObject.transform.Find("Model").localEulerAngles = new Vector3(0, angle, 0);

            Vector3 dir = PlayerModel.TransformDirection(Vector3.forward);

            CController.SimpleMove(dir * SPEED);
            
            charAnimation.SetBool("isRunning", true);

        }
        else
        {
            charAnimation.SetBool("isRunning", false);
            CController.SimpleMove(Vector3.zero);
        }
        
    }

}
