﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HealthManaBar : MonoBehaviour
{

    public Image ManaFG;
    public Image HealthFG;

    const float BarRotationSpeed = 15;

    // Update is called once per frame
    void Update()
    {

        float newFillValue = Mathf.Abs(Mathf.Sin(Time.time * 0.25f));

        ManaFG.transform.parent.GetComponent<Image>().fillAmount = newFillValue;
        HealthFG.transform.parent.GetComponent<Image>().fillAmount = newFillValue;


        ManaFG.rectTransform.Rotate(0, 0, BarRotationSpeed * Time.deltaTime);
        HealthFG.rectTransform.Rotate(0, 0, -BarRotationSpeed * Time.deltaTime);

    }


}
