﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeReducer : MonoBehaviour
{

    float currentTime = 60;

    void Update()
    {

        currentTime -= Time.deltaTime;

        if (currentTime <= 0)
            currentTime = 60;

        GetComponent<Text>().text = "Time left: " + (int)currentTime;

    }
}
