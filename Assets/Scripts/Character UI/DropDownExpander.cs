﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DropDownExpander : MonoBehaviour, IPointerClickHandler
{

    public RectTransform Container;

    public Image DropDownArrow = null;

    bool isOpen = false;

    public void OnPointerClick(PointerEventData eventData)
    {
        isOpen = !isOpen;
    }

    void Start()
    {
        Container = transform.Find("Container").GetComponent<RectTransform>();
    }

    void Update()
    {
        Vector3 scale = Container.localScale;
        scale.y = Mathf.Lerp(scale.y, isOpen ? 1 : 0, Time.deltaTime * 15);
        Container.localScale = scale;

        if (DropDownArrow != null)
        {
            Vector3 currentRot = DropDownArrow.rectTransform.localEulerAngles;
            float newZ = Mathf.LerpAngle(currentRot.z, (isOpen) ? -90 : 90, Time.deltaTime * 15);
            currentRot.z = newZ;

            DropDownArrow.rectTransform.localEulerAngles = currentRot;
            
        }
    }
}
