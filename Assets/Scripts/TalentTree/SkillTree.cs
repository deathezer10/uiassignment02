﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SkillTree : MonoBehaviour
{

    public float blueFireballPow_1 = 1;

    public Button btnBlueFire1;
    public Button btnBlueFire2;
    public Button btnBlueFire3;
    public Button btnBlueFire4;
    public Button btnBlueFire5;
    public Button btnBlueFire6;
    public Button btnBlueFire7;
    public Button btnBlueFire8;
    public Button btnBlueFire9;
    public Button btnBlueFire10;

    public Button btnRedFire1;
    public Button btnRedFire2;
    public Button btnRedFire3;
    public Button btnRedFire4;
    public Button btnRedFire5;
    public Button btnRedFire6;

    public Button btnPurpleFire1;
    public Button btnPurpleFire2;
    public Button btnPurpleFire3;
    public Button btnPurpleFire4;
    public Button btnPurpleFire5;
    public Button btnPurpleFire6;
    public Button btnPurpleFire7;
    public Button btnPurpleFire8;
    public Button btnPurpleFire9;
    public Button btnPurpleFire10;

    public int totalSkillPoints = 15;
    public Text SkillPointTxt;
    public int totatlSpentSP = 0;
    bool haveBlueFirelvl1 = false;
    bool haveBlueFirelvl2 = false;
    bool haveBlueFirelvl3 = false;
    bool haveBlueFirelvl4 = false;
    bool haveBlueFirelvl5 = false;
    bool haveBlueFirelvl6 = false;
    bool haveBlueFirelvl7 = false;
    bool haveBlueFirelvl8 = false;
    bool haveBlueFirelvl9 = false;
    bool haveBlueFirelvl10 = false;

    bool haveRedFireLvl1 = false;
    bool haveRedFireLvl2 = false;
    bool haveRedFireLvl3 = false;
    bool haveRedFireLvl4 = false;
    bool haveRedFireLvl5 = false;
    bool haveRedFireLvl6 = false;

    bool havePurpleFireLvl1 = false;
    bool havePurpleFireLvl2 = false;
    bool havePurpleFireLvl3 = false;
    bool havePurpleFireLvl4 = false;
    bool havePurpleFireLvl5 = false;
    bool havePurpleFireLvl6 = false;
    bool havePurpleFireLvl7 = false;
    bool havePurpleFireLvl8 = false;
    bool havePurpleFireLvl9 = false;
    bool havePurpleFireLvl10 = false;



    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Blue fire spells
        SkillPointTxt.text = "Skill Points: " + totalSkillPoints.ToString();

        if (totalSkillPoints == 0)
        {
            btnBlueFire1.interactable = false;
            //return;

            //  gameObject.particleSystem.enableEmission = false;
        }
        else
        {
            btnBlueFire1.interactable = true;
            //gameObject.particleSystem.enableEmission = true;
        }
        if (haveBlueFirelvl1 == false || totalSkillPoints == 0)
        {
            btnBlueFire2.interactable = false;
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnBlueFire2.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnBlueFire2.interactable = true;
        }
        if (haveBlueFirelvl2 == false || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnBlueFire3.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnBlueFire3.interactable = true;
        }
        if (haveBlueFirelvl3 == false || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnBlueFire4.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnBlueFire4.interactable = true;
        }
        if (haveBlueFirelvl1 == false || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnBlueFire5.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnBlueFire5.interactable = true;
        }
        if ((haveBlueFirelvl5 == false && haveBlueFirelvl2 == false) || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnBlueFire6.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnBlueFire6.interactable = true;
        }
        if ((haveBlueFirelvl6 == false && haveBlueFirelvl3 == false) || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnBlueFire7.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnBlueFire7.interactable = true;
        }
        if (haveBlueFirelvl5 == false || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnBlueFire8.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnBlueFire8.interactable = true;
        }
        if (haveBlueFirelvl8 == false || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnBlueFire9.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnBlueFire9.interactable = true;
        }
        if (haveBlueFirelvl8 == false || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnBlueFire10.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnBlueFire10.interactable = true;
        }



        //Red fire spells

        if (totalSkillPoints == 0)
        {
            btnRedFire1.interactable = false;
        }
        else
        {
            btnRedFire1.interactable = true;
        }
        if (haveRedFireLvl1 == false || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnRedFire2.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnRedFire2.interactable = true;
        }
        if (haveRedFireLvl2 == false || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnRedFire3.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnRedFire3.interactable = true;
        }
        if (haveRedFireLvl1 == false || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnRedFire4.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnRedFire4.interactable = true;
        }
        if ((haveRedFireLvl2 == false && haveRedFireLvl4 == false) || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnRedFire5.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnRedFire5.interactable = true;
        }
        if (haveRedFireLvl4 == false || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnRedFire6.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnRedFire6.interactable = true;
        }

        //Purple Fire talents

        if (totalSkillPoints == 0)
        {
            btnPurpleFire1.interactable = false;
        }
        else
        {
            btnPurpleFire1.interactable = true;
        }
        if (havePurpleFireLvl1 == false || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnPurpleFire2.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnPurpleFire2.interactable = true;
        }
        if (havePurpleFireLvl2 == false || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnPurpleFire3.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnPurpleFire3.interactable = true;
        }
        if (havePurpleFireLvl3 == false || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnPurpleFire4.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnPurpleFire4.interactable = true;
        }
        if (havePurpleFireLvl1 == false || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnPurpleFire5.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnPurpleFire5.interactable = true;
        }
        if ((havePurpleFireLvl5 == false && havePurpleFireLvl2 == false) || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnPurpleFire6.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnPurpleFire6.interactable = true;
        }
        if (havePurpleFireLvl3 == false || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnPurpleFire7.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnPurpleFire7.interactable = true;
        }
        if (havePurpleFireLvl5 == false || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnPurpleFire8.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnPurpleFire8.interactable = true;
        }
        if ((havePurpleFireLvl6 == false && havePurpleFireLvl8 == false) || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnPurpleFire9.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnPurpleFire9.interactable = true;
        }
        if (havePurpleFireLvl8 == false || totalSkillPoints == 0)
        {
            //btnBlueFire2.GetComponent<Button>().enabled = false;
            btnPurpleFire10.interactable = false;
        }
        else
        {
            // btnBlueFire2.GetComponent<Button>().enabled = true;
            btnPurpleFire10.interactable = true;
        }
    }



    //blue spells
    public void blueFireIncrLvl1()
    {
        if (totalSkillPoints == 0)
            return;
        if (haveBlueFirelvl1 == false)
        {
            haveBlueFirelvl1 = true;
            blueFireballPow_1 = 1.2f;
            totalSkillPoints -= 1;
            totatlSpentSP += 1;
            Debug.Log(blueFireballPow_1);
            btnBlueFire1.GetComponent<Image>().color = new Color(0, 1, 0);
        }

    }
    public void blueFireIncrLvl2()
    {
        if (totalSkillPoints == 0)
            return;
        if (haveBlueFirelvl2 == false)
        {
            haveBlueFirelvl2 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnBlueFire2.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }
    public void blueFireIncrLvl3()
    {
        if (totalSkillPoints == 0)
            return;
        if (haveBlueFirelvl3 == false)
        {
            haveBlueFirelvl3 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnBlueFire3.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }
    public void blueFireIncrLvl4()
    {
        if (totalSkillPoints == 0)
            return;
        if (haveBlueFirelvl4 == false)
        {
            haveBlueFirelvl4 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnBlueFire4.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }
    public void blueFireIncrLvl5()
    {
        if (totalSkillPoints == 0)
            return;
        if (haveBlueFirelvl5 == false)
        {
            haveBlueFirelvl5 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnBlueFire5.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }
    public void blueFireIncrLvl6()
    {
        if (totalSkillPoints == 0)
            return;
        if (haveBlueFirelvl6 == false)
        {
            haveBlueFirelvl6 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnBlueFire6.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }
    public void blueFireIncrLvl7()
    {
        if (totalSkillPoints == 0)
            return;
        if (haveBlueFirelvl7 == false)
        {
            haveBlueFirelvl7 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnBlueFire7.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }
    public void blueFireIncrLvl8()
    {
        if (totalSkillPoints == 0)
            return;
        if (haveBlueFirelvl8 == false)
        {
            haveBlueFirelvl8 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnBlueFire8.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }
    public void blueFireIncrLvl9()
    {
        if (totalSkillPoints == 0)
            return;
        if (haveBlueFirelvl9 == false)
        {
            haveBlueFirelvl9 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnBlueFire9.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }
    public void blueFireIncrLvl10()
    {
        if (totalSkillPoints == 0)
            return;
        if (haveBlueFirelvl10 == false)
        {
            haveBlueFirelvl10 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnBlueFire10.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }

    //Red fire spells
    public void redFireIncrLvl1()
    {
        if (totalSkillPoints == 0)
            return;
        if (haveRedFireLvl1 == false)
        {
            haveRedFireLvl1 = true;
            blueFireballPow_1 = 1.2f;
            totalSkillPoints -= 1;
            totatlSpentSP += 1;
            Debug.Log(blueFireballPow_1);
            btnRedFire1.GetComponent<Image>().color = new Color(0, 1, 0);
        }

    }
    public void redFireIncrLvl2()
    {
        if (totalSkillPoints == 0)
            return;
        if (haveRedFireLvl2 == false)
        {
            haveRedFireLvl2 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnRedFire2.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }
    public void redFireIncrLvl3()
    {
        if (totalSkillPoints == 0)
            return;
        if (haveRedFireLvl3 == false)
        {
            haveRedFireLvl3 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnRedFire3.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }
    public void redFireIncrLvl4()
    {
        if (totalSkillPoints == 0)
            return;
        if (haveRedFireLvl4 == false)
        {
            haveRedFireLvl4 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnRedFire4.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }
    public void redFireIncrLvl5()
    {
        if (totalSkillPoints == 0)
            return;
        if (haveRedFireLvl5 == false)
        {
            haveRedFireLvl5 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnRedFire5.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }
    public void redFireIncrLvl6()
    {
        if (totalSkillPoints == 0)
            return;
        if (haveRedFireLvl6 == false)
        {
            haveRedFireLvl6 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnRedFire6.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }

    //purple fire spells

    public void purpleFireIncrLvl1()
    {
        if (totalSkillPoints == 0)
            return;
        if (havePurpleFireLvl1 == false)
        {
            havePurpleFireLvl1 = true;
            blueFireballPow_1 = 1.2f;
            totalSkillPoints -= 1;
            totatlSpentSP += 1;
            Debug.Log(blueFireballPow_1);
            btnPurpleFire1.GetComponent<Image>().color = new Color(0, 1, 0);
        }

    }
    public void purpleFireIncrLvl2()
    {
        if (totalSkillPoints == 0)
            return;
        if (havePurpleFireLvl2 == false)
        {
            havePurpleFireLvl2 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnPurpleFire2.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }
    public void purpleFireIncrLvl3()
    {
        if (totalSkillPoints == 0)
            return;
        if (havePurpleFireLvl3 == false)
        {
            havePurpleFireLvl3 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnPurpleFire3.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }
    public void purpleFireIncrLvl4()
    {
        if (totalSkillPoints == 0)
            return;
        if (havePurpleFireLvl4 == false)
        {
            havePurpleFireLvl4 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnPurpleFire4.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }
    public void purpleFireIncrLvl5()
    {
        if (totalSkillPoints == 0)
            return;
        if (havePurpleFireLvl5 == false)
        {
            havePurpleFireLvl5 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnPurpleFire5.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }
    public void purpleFireIncrLvl6()
    {
        if (totalSkillPoints == 0)
            return;
        if (havePurpleFireLvl6 == false)
        {
            havePurpleFireLvl6 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnPurpleFire6.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }
    public void purpleFireIncrLvl7()
    {
        if (totalSkillPoints == 0)
            return;
        if (havePurpleFireLvl7 == false)
        {
            havePurpleFireLvl7 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnPurpleFire7.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }
    public void purpleFireIncrLvl8()
    {
        if (totalSkillPoints == 0)
            return;
        if (havePurpleFireLvl8 == false)
        {
            havePurpleFireLvl8 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnPurpleFire8.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }
    public void purpleFireIncrLvl9()
    {
        if (totalSkillPoints == 0)
            return;
        if (havePurpleFireLvl9 == false)
        {
            havePurpleFireLvl9 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnPurpleFire9.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }
    public void purpleFireIncrLvl10()
    {
        if (totalSkillPoints == 0)
            return;
        if (havePurpleFireLvl10 == false)
        {
            haveBlueFirelvl10 = true;
            blueFireballPow_1 = 1.3f;
            totalSkillPoints -= 1;
            totatlSpentSP += 2;
            Debug.Log(blueFireballPow_1);
            btnPurpleFire10.GetComponent<Image>().color = new Color(0, 1, 0);
        }
    }

}
