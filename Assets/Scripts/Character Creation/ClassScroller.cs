﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;


public class ClassScroller : MonoBehaviour
{

    public Text lblClassTitle;

    void Start()
    {
        SetClassName(GetComponent<HorizontalScrollSnap>().StartingScreen);
    }

    public void SetClassName(int pageIndex)
    {
        GameObject contentPane = GameObject.Find("Content");
        Image currentPageImage = contentPane.transform.GetChild(pageIndex).GetComponent<Image>();

        lblClassTitle.text = contentPane.transform.GetChild(pageIndex).name; // Set the selected class name to the label
        
        // Scale the main page up
        currentPageImage.rectTransform.localScale = new Vector3(1, 1, 1);
        currentPageImage.CrossFadeAlpha(1, 0, true);
        contentPane.transform.GetChild(pageIndex).GetComponent<Image>().CrossFadeAlpha(1, 0, true);

        // Blend alpha
        for (int i = 0; i < contentPane.transform.childCount; ++i)
        {
            if (i != pageIndex)
            {
                // Scale other pages down and lower their alpha
                Image imgPane = contentPane.transform.GetChild(i).GetComponent<Image>();
                imgPane.rectTransform.localScale = new Vector3(0.8f, 0.8f, 1);
                imgPane.CrossFadeAlpha(0.5f, 0.25f, false);
            }
        }

    }

}
