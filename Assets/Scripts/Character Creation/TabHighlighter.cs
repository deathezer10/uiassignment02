﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TabHighlighter : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (transform.childCount > 0)
        {

            for (int i = 0; i < transform.childCount; ++i)
            {
                if (i == transform.childCount - 1)
                    transform.GetChild(i).Find("Tab").GetComponent<Image>().CrossFadeColor(Color.white, 0.25f, false, false);
                else
                    transform.GetChild(i).Find("Tab").GetComponent<Image>().CrossFadeColor(Color.gray, 0.25f, false, false);
            }

        }


    }
}
