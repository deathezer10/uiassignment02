﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateCharacter : MonoBehaviour
{

    public GameObject ConfirmDialog;

    public void StartCreateCharacter(InputField txtfield)
    {
        // Check for empty and invalid characters
        if (txtfield.text.Trim() == "" || txtfield.text.Contains(" "))
        {
            ConfirmDialog.SetActive(true);
            return;
        }
            
        // Write new character to PlayerPrefs
        List<string> characters = new List<string>(PlayerPrefsX.GetStringArray("Characters"));
        characters.Add(txtfield.text.Trim());
        PlayerPrefsX.SetStringArray("Characters", characters.ToArray());

        // Go next scene
        GetComponent<SceneLoader>().LoadNextScene(1);
    }

}
