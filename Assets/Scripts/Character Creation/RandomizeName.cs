﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomizeName : MonoBehaviour {

	// Sets the given textfield text to a random name
    public void SetRandomizeName(InputField txtfield)
    {
        switch (Random.Range(0,7))
        {

            case 0:
                txtfield.text = "Spongebob";
                break;

            case 1:
                txtfield.text = "Patrick";
                break;

            case 2:
                txtfield.text = "Squidward";
                break;

            case 3:
                txtfield.text = "Plankton";
                break;

            case 4:
                txtfield.text = "Sandy";
                break;

            case 5:
                txtfield.text = "Mr.Krabs";
                break;

            case 6:
                txtfield.text = "Gary";
                break;

        }

    }

}
