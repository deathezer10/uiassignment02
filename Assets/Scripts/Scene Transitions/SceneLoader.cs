﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneLoader : MonoBehaviour {

       
    public void LoadNextScene(SceneFader fader)
    {
        StartCoroutine(LoadNextSceneRoutine(fader));
    }

     IEnumerator LoadNextSceneRoutine(SceneFader fader)
    {
        yield return new WaitForSeconds(fader.StartFade(true));
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void LoadNextScene(int sceneIndex)
    {
        StartCoroutine(LoadNextSceneIndexRoutine(sceneIndex));
    }

    IEnumerator LoadNextSceneIndexRoutine(int index)
    {
        var fader = GameObject.FindWithTag("MainCanvas").GetComponent<SceneFader>();
        yield return new WaitForSeconds(fader.StartFade(true));
        SceneManager.LoadScene(index);
    }


}