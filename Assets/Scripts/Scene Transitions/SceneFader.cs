﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneFader : MonoBehaviour
{

    public RawImage FadeTexture;
    const float FadeDuration = 1;

    void Start()
    {
        StartFade(false);

        if (SceneManager.GetActiveScene().name == "4_Gameplay")
            DestroyObject(GameObject.FindWithTag("UI_Rain"));

    }

    public float StartFade(bool fadeIn)
    {
        FadeTexture.enabled = true;
        FadeTexture.canvasRenderer.SetColor(new Color(1, 1, 1));

        if (fadeIn) // Transparent to opaque
        {
            FadeTexture.canvasRenderer.SetAlpha(0);
            FadeTexture.CrossFadeAlpha(1, FadeDuration, false);
        }
        else // Opaque to transparent
        {
            FadeTexture.canvasRenderer.SetAlpha(1);
            FadeTexture.CrossFadeAlpha(0, FadeDuration, false);
        }

        // return fade duration so that it could be used in conjunction with couroutines
        return FadeDuration;
    }

    void Update()
    {
        if (FadeTexture.canvasRenderer.GetAlpha() == 0 || FadeTexture.canvasRenderer.GetAlpha() == 1)
            FadeTexture.raycastTarget = false; // Disable touch interception
        else
            FadeTexture.raycastTarget = true; // Enable touch interception
    }
}
