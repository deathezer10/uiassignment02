﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterPortraitManager : MonoBehaviour
{

    public GameObject PortraitPrefab;
    public GameObject PortraitPositionPrefab;
    public CharDeleterDialog DeleteConfirmationDialog;

    void Start()
    {
        // Add some template characters on start

        AddPortrait("DexlessSin99");
        AddPortrait("PokerFace");
        AddPortrait("YourNeighbour");

        var characters = PlayerPrefsX.GetStringArray("Characters");

        foreach (string str in characters)
        {
            AddPortrait(str);
        }

    }

    // Adds the character portrait to the Content panel
    void AddPortrait(string name)
    {
        GameObject go = Instantiate(PortraitPrefab, transform);

        GameObject go2 = Instantiate(PortraitPositionPrefab, transform);
        go.GetComponent<CharacterPortrait>().Initialize(name, go2, DeleteConfirmationDialog);
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            AddPortrait("Player" + Random.Range(0, 100));
        }
    }

}
