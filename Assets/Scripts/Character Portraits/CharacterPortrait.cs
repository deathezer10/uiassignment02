﻿using UnityEngine;
using UnityEngine.UI;

public class CharacterPortrait : MonoBehaviour
{
    
    public Material PlayerHairMaterial;

    CharDeleterDialog ConfirmDialog;
    GameObject ReferencePoint;
    Vector2 velocity = Vector2.zero;

    string characterName;
    bool isHolding = false;
    float CurrentHoldDuration = 0;

    // Assign the reference target for this portrait to move to
    public void Initialize(string name, GameObject target, CharDeleterDialog confirmDialog)
    {
        GetComponent<RectTransform>().anchoredPosition = target.GetComponent<RectTransform>().anchoredPosition;
        ReferencePoint = target;
        GetComponentInChildren<Text>().text = characterName = name;
        ConfirmDialog = confirmDialog;
    }

    // Change the Current selected character name to this name
    public void SetTitleToName()
    {
        GameObject.Find("btnPlay").GetComponent<Image>().enabled = true;
        GameObject.Find("btnPlay").GetComponent<Button>().enabled = true;
        GameObject.Find("lblCharName").GetComponent<Text>().text = characterName;

        Color randColor = new Color();

        switch (Random.Range(0, 4))
        {
            case 0:
                randColor = Color.red;
                break;

            case 1:
                randColor = Color.blue;
                break;

            case 2:
                randColor = Color.green;
                break;

            case 3:
                randColor = Color.yellow;
                break;
        }



        PlayerHairMaterial.SetColor("_EmissionColor", randColor);
    }

    public void OnDeleteCharacter()
    {
        // Delete both reference and this object
        GameObject[] objects = { ReferencePoint, gameObject };
        ConfirmDialog.Show(objects);
    }

    public void StartHoldDuration()
    {
        isHolding = true;
    }

    public void StopHoldDuration()
    {
        isHolding = false;
        CurrentHoldDuration = 0;
        GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1); // Reset scaling
        transform.Find("Portrait").GetComponent<Image>().fillAmount = 1;
    }

    void Update()
    {
        // Smoothly translate to the new position over time
        RectTransform myRect = GetComponent<RectTransform>();
        myRect.anchoredPosition = Vector2.SmoothDamp(myRect.anchoredPosition, ReferencePoint.GetComponent<RectTransform>().anchoredPosition, ref velocity, 0.25f, Mathf.Infinity, Time.deltaTime);
    }

    void LateUpdate()
    {
        if (isHolding) // After X seconds, show the cross button
        {
            CurrentHoldDuration += Time.deltaTime;
            transform.Find("Portrait").GetComponent<Image>().fillAmount -= Time.deltaTime / 2;
            GetComponent<RectTransform>().localScale = new Vector3(0.9f, 0.9f, 1); // make the portrait smaller for visual feedback

            if (CurrentHoldDuration >= 2)
            {
                transform.Find("Cross").gameObject.SetActive(true); // Show cross
            }
        }
    }
}
