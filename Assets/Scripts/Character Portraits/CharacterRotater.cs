﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CharacterRotater : MonoBehaviour
{

    public GameObject PlayerModel;

    Text currentPlayerName;

    public void OnDrag(BaseEventData data)
    {
        var pointerData = (PointerEventData)data;

        PlayerModel.transform.Rotate(new Vector3(0, -pointerData.delta.x / 5, 0));
    }

    void Start()
    {
        currentPlayerName = GameObject.Find("lblCharName").GetComponent<Text>();
    }

    void Update()
    {
        if (currentPlayerName.text == "")
        {
            PlayerModel.SetActive(false);
        }
        else
        {
            PlayerModel.SetActive(true);
        }
    }

}
